#!/usr/bin/env nu

let interface = "wlan0"

def get-audio-output-icon [] {
	let muted = pactl get-sink-mute 0

	if $muted == "Mute: yes" {
		return "\u{f026}"		
	} else {
		return "\u{f028}"
	}
}

def get-mic-muted-icon [] {
	let muted = pactl get-source-mute 0

	if $muted == "Mute: yes" {
		return " \u{f036d}"
	}
}

def get-do-not-disturb-info [] {
	if (dunstctl is-paused | into bool) {
		let notification_count = dunstctl count waiting | into int
		
		if $notification_count != 0 {
			return $" \u{f009b} ($notification_count) "
		}

		return " \u{f009b} "
	}
}

def get-bluetooth-icon [] {
	let connected_devices = hcitool con | find ">" | length
	if ($connected_devices > 0) {
		return "\u{f00af} "
	}
}

def get-wifi-icon [] {
	let connected = do {
		iwctl station list
		| find $interface 
		| str contains "connected"
		| get 0
	}

	if $connected {
		return "\u{f16bd}"
	}

	return "\u{f05aa}"
}

def get-wifi-info [] {
	let wifi_name = do {
		iwconfig $interface
		| find ESSID
		| split row ":"
		| get 1
		| tr -d '"' 
		| into string
		| ansi strip
		| str trim
	}

	let signal_strength = do {
		iwconfig $interface 
		| find "Quality" 
		| split row "=" 
		| get 1 
		| parse "{value} {key}" 
		| get value 
		| get 0
	}

	return $"($wifi_name) \(($signal_strength)\)"
}

mut tick_tracker = 0
mut char_cursor = 0
mut current_track = (playerctl --player=spotify metadata -f "{{artist}} - {{title}}")
mut displayed_music_text = iterate_over_track $current_track $char_cursor
let ticks_per_update = 7

def iterate_over_track [data: string, cursor: int] {
	let max_char_len = 20

	let displayed_data = $data | str substring $cursor..($cursor + $max_char_len)

	return $displayed_data
}

while true {
	# I would love to clean this code up / put it into a separate function, but there is a bug preventing me from doing it
	# See https://github.com/nushell/nushell/issues/7862
	$current_track = (playerctl --player=spotify metadata -f "{{artist}} - {{title}}")
	$tick_tracker += 1
	if ($tick_tracker > $ticks_per_update) {
		$displayed_music_text = (iterate_over_track $current_track $char_cursor)
		$char_cursor += 1
		$tick_tracker = 0

		if ($current_track | str ends-with $displayed_music_text) {
			$char_cursor = 0
			$tick_tracker = $ticks_per_update * (-1)
		}
	}

	let volume = pactl get-sink-volume 0 | awk '{print $5}'
	print $"($displayed_music_text) (get-audio-output-icon) ($volume) (get-mic-muted-icon)(get-do-not-disturb-info) (get-bluetooth-icon)(get-wifi-icon) (get-wifi-info)"
	sleep 0.05sec
}
